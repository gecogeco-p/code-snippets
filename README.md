# Code Snippets

This is a public repository for storing code snippets of developers Gecogeco.
Such snippets are used for knowledge sharing, articles and wikis.

--

:link: https://www.gecogeco.com/
