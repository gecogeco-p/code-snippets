# Orginal source
# https://gitlab.com/gecogeco/lambda/-/blob/master/src/dynamic-monitoring/lambda_function.rb

require 'json'
require 'aws-sdk'

# Set notification destination and environment
TOPIC_ARN = ENV['TOPIC_ARN']
EC2_NAME = ENV['EC2_NAME']

# Create new instance of EC2 and CloudWatch
EC2 = Aws::EC2::Resource.new
CW = Aws::CloudWatch::Resource.new

# Main function
def lambda_handler(event:, context:)
    instance_id = event['detail']['EC2InstanceId']
    asg_name = event['detail']['AutoScalingGroupName']

    # Let's filter the request and check if we need to create or remove the dynamic alarm
    case event['detail-type']
        # When a new instance starts after scaling out
        when 'EC2 Instance Launch Successful'
            on_launched(instance_id, asg_name)
        # When an instance is deleted after scaling in
        when 'EC2 Instance Terminate Successful'
            on_terminated(instance_id)
    end
end

def on_launched(instance_id, asg_name)
    instance = EC2.instance(instance_id)

    return unless instance.exists?

    # CWAgent alarm for disk_used_percent
    if !CW.alarm("#{EC2_NAME}-disk-alarm-#{instance.id}").exists?
        CW.client.put_metric_alarm({
            alarm_name: "#{EC2_NAME}-disk-alarm-#{instance.id}",
            alarm_description: "Monitor CWAgent disk usage percentage if it's greater than 80%.",
            namespace: 'CWAgent',
            # Specify dimensions
            dimensions: [
                { name: 'InstanceId', value: instance.id },
                { name: 'AutoScalingGroupName', value: asg_name },
                { name: 'ImageId', value: instance.image_id },
                { name: 'InstanceType', value: instance.instance_type },
                { name: 'path', value: '/' },
                { name: 'device', value: 'nvme0n1p1' },
                { name: 'fstype', value: 'xfs' }
            ],
            metric_name: 'disk_used_percent',
            statistic: 'Average',
            threshold: 80,
            unit: 'Percent',
            comparison_operator: 'GreaterThanThreshold',
            period: 300,
            evaluation_periods: 1,
            datapoints_to_alarm: 1,
            treat_missing_data: 'notBreaching',
            alarm_actions: [ TOPIC_ARN ],
            ok_actions: [ TOPIC_ARN ],
        })
    end

    # CWAgent alarm for mem_used_percent
    if !CW.alarm("#{EC2_NAME}-memory-alarm-#{instance.id}").exists?
        CW.client.put_metric_alarm({
            alarm_name: "#{EC2_NAME}-memory-alarm-#{instance.id}",
            alarm_description: "Monitor CWAgent memory usage percentage if it's greater than 80%.",
            namespace: 'CWAgent',
            # Specify dimensions
            dimensions: [
                { name: 'InstanceId', value: instance.id },
                { name: 'AutoScalingGroupName', value: asg_name },
                { name: 'ImageId', value: instance.image_id },
                { name: 'InstanceType', value: instance.instance_type }
            ],
            metric_name: 'mem_used_percent',
            statistic: 'Average',
            threshold: 80,
            unit: 'Percent',
            comparison_operator: 'GreaterThanThreshold',
            period: 300,
            evaluation_periods: 1,
            datapoints_to_alarm: 1,
            treat_missing_data: 'notBreaching',
            alarm_actions: [ TOPIC_ARN ],
            ok_actions: [ TOPIC_ARN ],
        })
    end
end

# Delete alarm when instance is deleted
def on_terminated(instance_id)
    disk_alarm = CW.alarm("#{EC2_NAME}-disk-alarm-#{instance_id}")
    memory_alarm = CW.alarm("#{EC2_NAME}-memory-alarm-#{instance_id}")
    disk_alarm.delete if disk_alarm.exists?
    memory_alarm.delete if memory_alarm.exists?
end
